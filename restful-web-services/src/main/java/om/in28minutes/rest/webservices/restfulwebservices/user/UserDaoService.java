package om.in28minutes.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class UserDaoService {
	private static List<User> users = new ArrayList<User>();
	
	static{
		users.add(new User(1, "abc", new Date()));
		users.add(new User(2, "def", new Date()));
		users.add(new User(1, "ghi", new Date()));
	}
	
	private static int countUser = 3;
	
	public List<User> findAll()
	{
		return users;
	}
	public User findOne(int id)
	{
		for(User user: users)
		{
			if(user.getId()==id)
			{
				return user;
			}
		}
		return null;
	}
	public User save(User user)
	{
		if(user.getId()==null)
		{
			user.setId(++countUser);
			users.add(user);
			return user;
		}
		return null;
	}
	public User deleteUser(int id)
	{
		Iterator<User> iterator = users.iterator();
		while(iterator.hasNext())
		{
			User user = iterator.next();
			{
				if(user.getId()==id)
				{
					iterator.remove();
					return user;
				}
			}
		}
		return null;
	}

}

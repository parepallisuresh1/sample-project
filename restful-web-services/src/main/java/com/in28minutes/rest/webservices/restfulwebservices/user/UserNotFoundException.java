package com.in28minutes.rest.webservices.restfulwebservices.user;

public interface UserNotFoundException {

	String getMessage();

}
